import java.util.Random;

public class Fourth {
    public static void main(String[] args) {
        int[] array = new int[8];
        Random r = new Random();
        int x = r.nextInt(10) + 1;
        for (int i = 0; i < array.length; i++)
            array[i] = r.nextInt(10) + 1;
        for (int i = 0; i < array.length; i++)
            System.out.print(array[i] + " ");
        boolean check = true;
        for (int i=0; i<array.length-1; i++)
            if (array[i] > array[i+1]) {
                check = false;
                break;
            }
        System.out.println();
        if (check)
            System.out.println("The array is a strictly increasing sequence.");
        else
            System.out.println("The array is not a strictly increasing sequence.");
        for (int i = 0; i < array.length; i++)
            if (i % 2 != 0)
                array[i] = 0;
        for (int i = 0; i < array.length; i++)
            System.out.print(array[i] + " ");
    }
}
